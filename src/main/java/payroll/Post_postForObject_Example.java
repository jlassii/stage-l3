package payroll ;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;

public class Post_postForObject_Example {

    static final String URL_CREATE_EMPLOYEE = "http://localhost:8080/employees";

    public static void main(String[] args) {

        int id = 2;
        Employee newEmployee = new Employee("Emna", "RH");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restTemplate = new RestTemplate();

        // Data attached to the request.
        HttpEntity<Employee> requestBody = new HttpEntity<>(newEmployee, headers);

        // Send request with POST method.
        Employee e = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, Employee.class);
        e.setId((long) 2);

        if (e != null && e.getId() != null) {

            System.out.println("Employee created: " + e.getName());
        } else {
            System.out.println("Something error!");
        }

    }

}